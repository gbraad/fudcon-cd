Reveal.initialize({
    controls: false,
    progress: true,
    history: false,
    keyboard: true,
    overview: true,
    center: true,
    loop: false,
    rtl: false,
    autoSlide: 0,
    mouseWheel: false,
    rollingLinks: false,
    transition: 'none', // default/cube/page/concave/zoom/linear/fade/none
    transitionSpeed: 'default', // default/fast/slow
});